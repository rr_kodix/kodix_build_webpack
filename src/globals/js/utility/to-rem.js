/**
 * Created by Kodix on 31.01.2017.
 */
// const rootfz = parseFloat(window.getComputedStyle(document.documentElement).fontSize);

export default function (pixels) {
  return parseFloat(pixels) / 10;
}
