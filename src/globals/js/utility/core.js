/**
 * Created by Kodix on 01.02.2017.
 */

export function assert(condition, message = 'Assertion failed') {
  if (!condition && process.env.NODE_ENV !== 'production') {
    if (typeof Error !== 'undefined') {
      throw new Error(message);
    }
    throw message; // Fallback
  }
}

export function devlog(variable) {
  if (process.env.NODE_ENV === 'development') {
    console.log(variable);
  }
}


// Returns true if it is a DOM node
export function isNode(o) {
  return (
    typeof Node === 'object' ? o instanceof Node :
    o && typeof o === 'object' && typeof o.nodeType === 'number' && typeof o.nodeName==='string'
  );
}

// Returns true if it is a DOM element
export function isElement(o) {
  return (
    typeof HTMLElement === 'object' ? o instanceof HTMLElement : // DOM2
    o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName==='string'
  );
}
