import $ from 'jquery';
import Page from './page';


export default class Component {
  constructor(element) {
    this.componentElement = element;
    this.pageIndex = Array.prototype.indexOf.call(element.parentNode.children, element);

    this.componentElement._component = this;
    $(this.componentElement).trigger('_component:init', this);
  }
  initComponent() {
    this.id = Page.registerModule(this);
  }
  updatePageIndex() {
    this.pageIndex = Array.prototype.indexOf.call(this.componentElement.parentNode.children, this.componentElement);
    Page.sortModules();
  }
}

export function findComponent(selector, Instance = Component) {
  $(() => {
    $(selector).each((index, el) => {
      const thisInstance = new Instance(el);
      if (Instance === Component) {
        thisInstance.initComponent();
      }
    });
  });
}

