import $ from 'jquery';
// import {addClass, removeClass, hasClass} from './classesMunipulation';

export const validateRegExp = {
  'field-type-email': /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
  'field-type-phone': /^((\+8|\+7|8)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/,
  'field-type-time': /^([01]\d|2[0-3]):([0-5]\d)$/,
  'field-type-inn': /^[0-9]{10,12}$/,
  'field-type-bik': /^[0-9]{9}$/,
  'field-type-correspondent_account': /^[0-9]{20}$/,
  'field-type-payment_account': /^[0-9]{20}$/,
  'field-type-text': '',
  'field-type-number': /^[0-9]{1,}$/,
  'field-type-date': /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
  'field-type-vin': '^[^\\Wioq]{17}$',
  'field-type-fio': `^[-'a-zA-ZА-я]+$`,
};

export class FormValidator {
  constructor(form) {
    if(!$(form).length) {
      throw new Error('There is no form to validate');
    }
    this.form = form;
    this.$form = $(form);
  }

  // const additionalFormValidator=false;

  isValid() {
    let formValid = true;
    $.each($(this.$form).find(':input'), function(i, inp) {
      if (!validateField(inp)) {
        formValid = false;
      }
    });
    this.$form.trigger('kdxFormValidator', [formValid]);
    return formValid;
  };

  setValidateReg(className, regExp) {
    validateRegExp[className]=regExp;
  };

  getValidateReg(className) {
    return validateRegExp[className];
  };

  setAdditionalValidator(f) {
    additionalFormValidator=f;
  };
}

export function validateField(field) {
  if($(field).is(':disabled'))
    return true;

  let isValid = true;
  if($(field).is(':checkbox') && $(field).hasClass('required') && !$(field).prop('checked')) {
    isValid = false;
  }

  if($(field).is(':radio') && $(field).hasClass('required')) {
    if(!$(`[name=${$(field).attr('name')}]:checked`).length) {
      isValid = false;
    }
  }

  if($(field).hasClass('required') && !$.trim($(field).val())) {
    isValid = false;
  }

  $.each(validateRegExp, function(className, reg) {
    if($(field).hasClass(className) && ($(field).hasClass('required') || $.trim($(field).val())) ) {
      if(!String($(field).val()).match(reg)) {
        isValid = false;
      }
    }
  });

  // $(field).trigger('kdxFieldValidator', [isValid]);
  return isValid;
};

export function addValidationOnChange(el) {
  const $errContainer = $(el).closest('.js_error_container');
  if(!$(el).hasClass('required')) return false;
  if($errContainer.find('.required-mark').length === 0) {
    const zvezdochka = $('<sup/>').text('*').addClass('required-mark'); // Samaya krasivaya zvezdochka moya!
    $('label', $errContainer).append(zvezdochka);
  }
  $(el).on('change blur dp.change', ()=> {
    const isValid = validateField(el);
    if(isValid) {
      $errContainer.removeClass('has-error');
      $errContainer.addClass('field-valid');
    } else {
      $errContainer.addClass('has-error');
      $errContainer.removeClass('field-valid');
    }
  });
  el.addEventListener('focus', () => {
    $errContainer.removeClass('field-valid');
  });
}

export function clearForm(form) {
  // return $.each(function() {
  //   const type = this.type, tag = this.tagName.toLowerCase();
  //   if(this.disabled){
  //     return;
  //   }
  //   if (tag == 'form')
  //     return $(':input',this).clearForm();
  //   if (type == 'text' || type == 'password' || tag == 'textarea')
  //     this.value = '';
  //   else if (type == 'checkbox' || type == 'radio')
  //     this.checked = false;
  //   else if (tag == 'select')
  //     this.selectedIndex = 0;
  // });
};

// $(document).on('click', '.reset_custom_form', function(){
//   const form=$(this).closest('form');
//   $(form).clearForm();
//   $(form).find('.inp_checkbox').removeClass('checked');
//   $(form).find('.js_chosen_select').trigger('chosen:updated');
//   return false;
// });
