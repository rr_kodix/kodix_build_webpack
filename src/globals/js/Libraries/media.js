import $ from 'jquery';

import {addClass, removeClass} from '../utility/classesMunipulation';

import _map from 'lodash/map';
import _mapValues from 'lodash/mapValues';
import _each from 'lodash/each';
import _first from 'lodash/first';

class Media {
  constructor() {
    const self = this;
    this.breakpoints = {
      phone: 320,
      fablet: 480,
      tablet: 768,
      laptop: 1024,
      desktop: 1281,
      widescreen: 1486,
    };
    this.matches = {}; // https://developer.mozilla.org/ru/docs/Web/API/Window/matchMedia
    this.device = '';
    this.isMobile = true;
    this.isTouch = false;

    this.updateDeviceClass();

    $(window).resize(() =>
    self.updateDeviceClass()
  );
    $(document).ready(() =>
    self.updateDeviceClass()
  );
  }

  updateDeviceClass() {
    const wW = window.innerWidth;
    const self = this;
    const sortedBreakpoints = (_map(this.breakpoints, (v, key) => [key, v]))
  .sort((a, b) => a[1] - b[1]);
    const oldDevice = self.device;
    self.isMobile = wW < this.breakpoints.tablet;

    _each(sortedBreakpoints, (v) => {
      if (wW > v[1]) {
      self.device = v[0];
    }
  });

    // fallback to 'phone' on sub-phone widths
    self.device = self.device ? self.device : _first(sortedBreakpoints)[0];

    if (!!oldDevice && oldDevice !== self.device) {
      self.emitDeviceChange();
    }
    this.updateMatches.bind(this)();
    this.updateTouchSupport.bind(this)();
  }

  emitDeviceChange() {
    const self = this;
    const event = new CustomEvent('deviceChanged', {
      detail: {
        device: self.device,
      },
    });
    document.dispatchEvent(event);
  }

  updateMatches() {
    this.matches = _mapValues(this.breakpoints, (bp)=> window.matchMedia(`(min-width: ${bp}px)`).matches);
  }

  updateTouchSupport() {
    let touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints>0) || (navigator.msMaxTouchPoints>0);
    const html = document.querySelector('html');
    if (!touchsupport) { // browser doesn't support touch
      addClass(html, 'no-touch');
      this.isTouch = false;
    } else { // browser supports touch
      removeClass(html, 'no-touch');
      this.isTouch = true;
    }
  }
}

export default new Media();
