let instance = null;


class Page {
  constructor() {
    if (!instance) {
      instance = this;
    }
    this.modules = [];
    this.props = {};
    document.addEventListener('DOMContentLoaded', () => document.body.style.visibility = '');
    return instance;
  }

  registerModule(module) {
    const id = this.modules.length;
    this.modules.push(module);
    this.sortModules();
    return id;
  }

  /**
   *
   * @param {Element} el
   * @returns {Promise}
   */
  getModuleByEl(el) {
    return new Promise((resolve, reject) => {
      if(typeof el === 'undefined') reject('undefined DOM element');
      if(typeof el._component === 'undefined') {
        el.addEventListener('_component:init', function() {
          resolve(el._component);
        });
      } else {
        resolve(el._component);
      }
    });
  }

  sortModules() {
    this.modules = this.modules.sort((a, b) => a.pageIndex - b.pageIndex);
    this.applyColors();
  }

  /**
   * swap dom elements of modules
   * @param module1 {Component}
   * @param module2 {Component}
   */
  swapModules(module1, module2) {
    const el1 = module1.componentElement;
    const el2 = module2.componentElement;
    // save the location of el2
    const parent2 = el2.parentNode;
    const next2 = el2.nextSibling;
    // special case for el1 is the next sibling of el2
    if (next2 === el1) {
      // just put el1 before el2
      parent2.insertBefore(el1, el2);
    } else {
      // insert el2 right before el1
      el1.parentNode.insertBefore(el2, el1);

      // now insert el1 where el2 was
      if (next2) {
        // if there was an element after el2, then insert el1 right before that
        parent2.insertBefore(el1, next2);
      } else {
        // otherwise, just append as last child
        parent2.appendChild(el1);
      }
    }

    module1.updatePageIndex();
    module2.updatePageIndex();
  }
}

export default new Page();


