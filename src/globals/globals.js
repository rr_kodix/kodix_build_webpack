// variables, mixins and global styles for whole project
import 'globals/style/common.scss';

if (process.env.NODE_ENV !== 'production') {
  require('./js/_basisCss');
}
import './js/_core';
