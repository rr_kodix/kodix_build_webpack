// import {findComponent} from '../../../globals/js/Libraries/component';
import $ from 'jquery';
// Import styles
import './index.scss';

const $spoiler = $('.spoiler');

$('.spoiler__title', $spoiler).on('click', (e) => {
    const $target = $(e.currentTarget);
    const $parent = $target.closest('.spoiler');

    if ($parent.hasClass('spoiler_open')) {
        $parent.find('.spoiler__text').slideUp();
        $parent.removeClass('spoiler_open');
    } else {
        $parent.find('.spoiler__text').slideDown();
        $parent.addClass('spoiler_open');
    }
});

// findComponent('COMPONENT_SELECTOR');
