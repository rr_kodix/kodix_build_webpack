import $ from 'jquery';
import Media from '../../globals/js/Libraries/media';

import '../../globals/globals';
import './index.scss';

import '../../components/MasterPageItem/index';


if (module.hot) {
  module.hot.accept();
}

// show errors on form elements
$('.js_masterpage_add_error').find('.js_error_container').addClass('has-error');

$(document).on('deviceChanged', () => {
  console.log('Device: ', Media.device);
  console.log('isMobile: ', Media.isMobile);
  console.log('----------------------------');
});
