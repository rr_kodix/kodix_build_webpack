import $ from 'jquery';
import Media from '../../globals/js/Libraries/media';

import '../../globals/globals';
import './index.scss';


document.addEventListener('deviceChanged', () => {
  console.log('Device: ', Media.device);
  console.log('isMobile: ', Media.isMobile);
  console.log('----------------------------');
});
