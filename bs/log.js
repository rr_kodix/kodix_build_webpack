/**
 * Created by UFS on 3/11/2017.
 */
import path from 'path';
import fs from 'fs';
import rimraf from 'rimraf';

export default function (name, contents, json = true) {
    if (json) {
        fs.writeFileSync(path.resolve(__dirname, 'logs', `${name}.json`), JSON.stringify(contents, null, 2));
    } else {
        fs.writeFileSync(path.resolve(__dirname, 'logs', `${name}.log`), contents);
    }
}

export function logsClean() {
    const logsFolder = path.resolve(__dirname, 'logs');
    if(fs.existsSync(logsFolder)) {
        rimraf.sync(logsFolder);
        fs.mkdirSync(logsFolder);
    } else {
        fs.mkdirSync(logsFolder);
    }

}