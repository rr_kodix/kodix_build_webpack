/**
 * Created by UFS on 3/5/2017.
 * Init babel for core scripts
 */

var config = {
    "presets": [ "es2015", "stage-0", "react"],

    /* if you want to use babel runtime, uncomment the following line */
    // "plugins": ["transform-runtime"],

    "env": {
        "build": {
            "optional": ["optimisation", "minification"]
        }
    }
};

require("babel-core/register")(config);
require("../core");