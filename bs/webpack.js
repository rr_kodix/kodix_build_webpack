/**
 * Created by UFS on 3/11/2017.
 */
/*
 IMPORTS
 */
// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';

// Utility packages
import _ from 'lodash';
import glob from 'glob';
import rimraf from 'rimraf';
import log, {logsClean} from './log';

// Webpack
import webpack from 'webpack';
import webpackMerge from 'webpack-merge';

// Webpack configs
import DLLConfig from './webpack/_dll';
import BasisCssConfig from './webpack/basisCss';
import DLLRefConfig from './webpack/_dllReference';
import ResolveConfig from './webpack/resolve';
import CommonConfig from './webpack/common';
import KodixToolsConfig from './webpack/kodix';
import HtmlConfig from './webpack/html';
import DevelopmentConfig from './webpack/development';
import LoadersConfig from './webpack/loaders';
import MinifyConfig from './webpack/minify';

// Dev Server
import browserSync from 'browser-sync';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

// NodeEnvironment
import NODE_ENV from './environment.js';

/*
 CONSTANTS
 */
const webpackStatsConfig = {
  chunks: false, // Makes the build much quieter
  cached: false,
  modules: false,
  colors: true
};

// Additional arguments
const argv = yargs.argv;

function cleanBuildFolder(CONFIG) {
  const buildFolder = path.resolve(__dirname, `../${CONFIG.paths.output}`)
  rimraf.sync(buildFolder);
  fs.mkdirSync(buildFolder);
}

function webpackMain(CONFIG) {
  console.log('Launching webpack for main files.\n');
  const outputPath = path.resolve(__dirname, '../', CONFIG.paths.output);

  let webpackConfig = webpackMerge(CommonConfig(CONFIG), ResolveConfig(CONFIG), LoadersConfig(CONFIG));
  if(!argv.noHtml) {
    webpackConfig = webpackMerge(webpackConfig, HtmlConfig(CONFIG));
  }
  if (CONFIG.hasOwnProperty('libs') && CONFIG.libs.length > 0) {
    webpackConfig = webpackMerge(webpackConfig, DLLRefConfig(CONFIG));
  }
  if (CONFIG.hasOwnProperty('basisCss') && CONFIG.basisCss.length > 0) {
    webpackConfig = webpackMerge(webpackConfig, BasisCssConfig(CONFIG));
  }
  if (CONFIG.devServer.active) {
    webpackConfig = webpackMerge(webpackConfig, DevelopmentConfig(CONFIG));
  }
  if (CONFIG.kdxTools) {
    webpackConfig = webpackMerge(webpackConfig, KodixToolsConfig(CONFIG));
  }

  if (CONFIG.minify) {
    webpackConfig = webpackMerge(webpackConfig, MinifyConfig(CONFIG));
  }

  if (argv.debug) {
    log('webpack_main', webpackConfig);
  }

  const webpackMainInstance = webpack(webpackConfig);
  if (!CONFIG.devServer.active) {
    if(!argv.watch) {
      webpackMainInstance.run(function (err, stats) {
        if (err) {
          console.log(err);
          return;
        }
        console.log(stats.toString(webpackStatsConfig));
        if (argv.log) {
          log('main_build', stats.toString(webpackStatsConfig), false);
        }
      });
    } else {
      webpackMainInstance.watch({ // watch options:
        aggregateTimeout: 300, // wait so long for more changes
        poll: true // use polling instead of native watchers
        // pass a number to set the polling interval
      }, function (err, stats) {
        if (err) {
          console.log(err);
          return;
        }
        console.log(stats.toString(webpackStatsConfig));
        if (argv.log) {
          log('main_build', stats.toString(webpackStatsConfig), false);
        }
      });
    }
  } else {
    browserSync({
      server: {
        baseDir: CONFIG.paths.output,

        middleware: [
          webpackDevMiddleware(webpackMainInstance, {
            // IMPORTANT: dev middleware can't access config, so we should
            // provide publicPath by ourselves
            publicPath: webpackConfig.output.publicPath,

            // pretty colored output
            stats: webpackStatsConfig

            // for other settings see
            // http://webpack.github.io/docs/webpack-dev-middleware.html
          }),

          // bundler should be the same as above
          webpackHotMiddleware(webpackMainInstance)
        ]
      },

      port: CONFIG.devServer.port,

      // no need to watch '*.js' here, webpack will take care of it for us,
      // including full page reloads if HMR won't work
      files: [
        // `**/*.css`,
        `**/*.html`,
      ]
    });
  }

}

function webpackDll(CONFIG) {
  console.log('Launching webpack for DLL\'s \n');
  let webpackConfig = webpackMerge(DLLConfig(CONFIG), ResolveConfig(CONFIG), LoadersConfig(CONFIG));
  if (CONFIG.minify) {
    webpackConfig = webpackMerge(webpackConfig, MinifyConfig(CONFIG));
  }
  if (argv.debug) {
    log('webpack_dll', webpackConfig);
  }
  const webpackDllInstance = webpack(webpackConfig);
  webpackDllInstance.run(function (err, stats) {
    if (err) {
      console.log(err);
      if (argv.log) {
        log('dll_build_error', err, false);
      }
      return;
    }

    console.log(stats.toString(webpackStatsConfig));

    webpackMain(CONFIG);
  });
}


export default function (CONFIG) {
  cleanBuildFolder(CONFIG);
  if (!CONFIG.hasOwnProperty('libs') || CONFIG.libs.length === 0) {
    webpackMain(CONFIG);
  } else {
    webpackDll(CONFIG);
  }
}
