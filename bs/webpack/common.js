/**
 * Created by UFS on 3/11/2017.
 */
// Common packages
import path from 'path';

// Utility packages
import _ from 'lodash';
import glob from 'glob';

// Webpack instance
import webpack from 'webpack';

// Builder config
import bsCONFIG from '../config.json';

// NodeEnvironment
import NODE_ENV from '../environment.js';

// Webpack plugins
import AssetsPlugin from 'assets-webpack-plugin';
import WriteFilePlugin from 'write-file-webpack-plugin';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';

import entriesObject from './utils/entries';


export default function (CONFIG) {
  const PATHS = CONFIG.paths;

  // Get all entries in target path
  let entries = entriesObject(CONFIG);

  return {
    entry: entries,
    output: {
      path: path.resolve(process.cwd(), CONFIG.paths.output),
      publicPath: CONFIG.paths.publicPath,
      filename: CONFIG.paths.assets.js + '[name]' + (CONFIG.paths.hashes ? '.[chunkhash]' : '') + '.js',
      // hotUpdateChunkFilename: CONFIG.paths.assets.js + '[name].[hash].hot.js'
    },
    plugins: [
      new webpack.DefinePlugin(_.assign({
        'process.env': {
          NODE_ENV: JSON.stringify(NODE_ENV),
          PUBLIC_PATH: JSON.stringify(CONFIG.paths.publicPath)
        },
        KDX_TOOLS: CONFIG.kdxTools
      }, _.mapValues(CONFIG.provide, function (v) {
        return JSON.stringify(v);
      }))),
      ...(NODE_ENV === 'development' ? [] : [new webpack.optimize.CommonsChunkPlugin({
        name: 'commons',
        filename: CONFIG.paths.assets.js + 'commons' + (CONFIG.paths.hashes ? '.[hash]' : '') + '.js',
        minChunks: 2
      })]),
      new AssetsPlugin({
        filename: 'assets.json',
        path: path.resolve(CONFIG.paths.output),
        prettyPrint: true
      }),
      new ProgressBarPlugin(),
      new WriteFilePlugin({
        test: /\.(css|html)$/,
        log: false
      }),
    ],
    devtool: !CONFIG.minify && CONFIG.devServer.active ? bsCONFIG.webpackSourceMapType : false
  }
}
