/*
 * Pug loader config
 */
import path from 'path';

module.exports = function (CONFIG) {
  return {
    test: /\.pug/,
    loader: 'pug-loader',
    options: {
      pretty: true,
      root: path.resolve(process.cwd(), CONFIG.paths.sourceRoot)
    }
  };
};
