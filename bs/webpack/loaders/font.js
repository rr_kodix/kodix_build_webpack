/*
 * Font loader config
 */

module.exports = function (CONFIG) {
  return {
    test: /\.(ttf|eot|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    use: [
      {
        loader: "url-loader",
        options: {
          limit: 100,
          name: CONFIG.paths.assets.fonts + '[name]' + (CONFIG.paths.hashes ? '.[hash]' : '') + '.[ext]'
        }
      }
    ]
  }
};
