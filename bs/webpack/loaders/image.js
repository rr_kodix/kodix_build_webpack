/*
 * Image loader config
 */

module.exports = function (CONFIG) {
  return {
    test: /\.(jpe?g|png|gif|svg)$/i,
    use: !CONFIG.devServer.active ? [
        {
          loader: "url-loader",
          options: {
            limit: 100,
            name: CONFIG.paths.assets.images + '[name]' + (CONFIG.devServer.active ? '.[hash]' : '') + '.[ext]'
          }
        },
        {
          loader: "image-webpack-loader",
          options: {
            mozjpeg: {
              progressive: true,
            },
            gifsicle: {
              interlaced: false,
            },
            optipng: {
              optimizationLevel: 4,
            },
            pngquant: {
              quality: '75-90',
              speed: 3,
            },
          },
        }
      ] : [
        {
          loader: "url-loader",
          options: {
            limit: 100,
            name: CONFIG.paths.assets.images + '[name]' + (CONFIG.paths.hashes ? '.[hash]' : '') + '.[ext]'
          }
        }
      ]
  }
};

