/*
 * JS/JSX loader config
 */

import path from 'path';

function escape(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

module.exports = function (CONFIG) {
  return {
    test: /\.js[x]?$/,
    exclude: /node_modules/,
    use: [
      'imports-loader?$=jquery,jQuery=jquery',
      {
        loader: 'babel-loader',
        options: {
          presets: ['es2015'],
          plugins: ['transform-runtime'],
          cacheDirectory: true,
        }
      },
      'eslint-loader'
    ]
  };
};
