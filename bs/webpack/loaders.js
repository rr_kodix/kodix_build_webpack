/**
 * Created by UFS on 3/11/2017.
 */
// Common packages
import fs from 'fs';
import path from 'path';
import yargs from 'yargs';

// Utility packages
import _ from 'lodash';
import util from 'util';
import glob from 'glob';

// NodeEnvironment
import NODE_ENV from '../environment.js';

// Webpack
import webpack from 'webpack';
import webpackMerge from 'webpack-merge';

// Webpack plugins
import ExtractTextPlugin from 'extract-text-webpack-plugin';

// PostCSS plugins list
import autoprefixer from 'autoprefixer';

// Builder config
import bsCONFIG from '../config.json';

export default function (CONFIG) {
    const sassExtract = new ExtractTextPlugin({
        filename: CONFIG.paths.assets.css + '[name]' + (CONFIG.paths.hashes ? '.[contenthash]' : '') + '.css',
        allChunks: true,
        disable: CONFIG.devServer.active,
    });

    const LOADERS = {
        module: {
            rules: _.flatten(_.map(glob.sync('./loaders/*.js', {
                cwd: __dirname
            }), function (filePath) {
                return require(filePath)(CONFIG);
            }))
        }
    };

    return webpackMerge({
            module: {
                rules: [
                    {
                        test: /\.scss$/,
                        use: sassExtract.extract({
                            fallback: 'style-loader',
                            use: [
                                {
                                    loader: 'css-loader',
                                    options: {
                                        sourceMap: !CONFIG.minify && !CONFIG.devServer.active,
                                        minimize: !CONFIG.devServer.active
                                    }
                                },
                                {
                                    loader: 'postcss-loader',
                                    options: {
                                        plugins: [
                                            autoprefixer({browsers: bsCONFIG.autoprefixer})
                                        ]
                                    }
                                },
                                'resolve-url-loader',
                                {
                                    loader: 'sass-loader',
                                    options: {
                                        sourceMap: true,
                                        data: `$env: ${NODE_ENV};`
                                    }
                                }
                            ]
                        })
                    }
                ]
            },
            plugins: [
                sassExtract
            ]
        },
        LOADERS);
}
