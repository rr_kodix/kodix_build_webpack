/**
 * Created by UFS on 3/12/2017.
 */
// Common packages
import path from 'path';

// Utility packages
import _ from 'lodash';
import glob from 'glob';

export default function (CONFIG) {
    const devHMR = CONFIG.devServer.active ? [
            'webpack-hot-middleware/client?reload=true',
        ] : [];

    return _.mapValues(_.keyBy(_.map(glob.sync(CONFIG.paths.entries + '*/*.@(js)', { //'*/*.@(js|ts)' when ts will be added
        cwd: path.resolve(__dirname, '../../../')
    }), function (filePath) {
        const parsedPath = path.parse(filePath);
        const name = parsedPath.dir.replace(CONFIG.paths.entries, '');

        return {
            name: name,
            path: path.resolve(__dirname, '../../../', filePath)
        };
    }), (o) => o.name),
        (o) => [...devHMR, o.path]);
}