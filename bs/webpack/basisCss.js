// Common packages
import path from 'path';

// Builder config
import bsCONFIG from '../config.json';

// Webpack instance
import webpack from 'webpack';

// Webpack plugins
import AssetsPlugin from 'assets-webpack-plugin';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';


export default function (CONFIG) {
    const outputPath = path.resolve(__dirname, '../../', CONFIG.paths.output);

    return {
        entry: {
            basisCss: CONFIG.basisCss,
        },
        output: {
            path: outputPath,
            publicPath: CONFIG.paths.publicPath,
            filename: CONFIG.paths.assets.js + '[name]' + (CONFIG.paths.hashes ? '.[chunkhash]' : '') + '.js',
            library: 'basisCss'
        },
        plugins: [
            // new webpack.DllPlugin({
            //     name: 'basisCss',
            //     path: path.resolve(CONFIG.paths.output, '[name]-manifest.json')
            // }),
            new AssetsPlugin({
                filename: 'basisCss-assets.json',
                path: path.resolve(CONFIG.paths.output),
                prettyPrint: true
            }),
            new ProgressBarPlugin(),
        ],
        devtool: !CONFIG.minify && CONFIG.devServer.active ? bsCONFIG.webpackSourceMapType : false
    };
}
