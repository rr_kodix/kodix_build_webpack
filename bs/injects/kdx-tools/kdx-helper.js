/* eslint-disable */

require('./kdx-helper.scss');

// Polyfills
require('./polyfills/object.keys');

var d = document;
var jsCookie = require('js-cookie');

// ====================================
// KDX MENU [dynacmic]
// ====================================
var kdxNav = d.createElement('div');
var kdxNavTrigger = d.createElement('div');
var kdxNavBody = d.createElement('div');
var kdxNavUtils = d.createElement('div');

document.addEventListener('DOMContentLoaded', function () {
  kdxNavUtils.className = 'kdx-nav__utils';
  kdxNav.appendChild(kdxNavUtils);

  kdxNav.className = 'kdx-nav';

  kdxNavTrigger.className = 'kdx-nav__trigger';
  kdxNavTrigger.innerHTML = 'Kodix Menu';
  kdxNav.appendChild(kdxNavTrigger);

  kdxNavBody.className = 'kdx-nav__body';
  for (var i = 0; i < PAGES.length; i++) {
    var kdxLink = d.createElement('a');
    kdxLink.href = PAGES[i].publicPath;
    kdxLink.innerHTML = PAGES[i].name;
    kdxNavBody.appendChild(kdxLink);
  }
  kdxNav.appendChild(kdxNavBody);

  d.body.appendChild(kdxNav);


  kdxNavTrigger.addEventListener('click', function (e) {
    e.preventDefault();

    kdxNav.classList.toggle('kdx-nav--opened');
    kdxNavBody.style.height = !kdxNavBody.style.height ? (PAGES.length * 24 + 'px') : '';
  })
}, false);


// Подсветка кода, чтобы заработало, нужно подключить highlight.js, например отсюда — https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.9.0/highlight.min.js
try {
  hljs.initHighlightingOnLoad();
} catch (err) {
  console.warn('highlite.js is not included, you can\'t use code highlite here :(');
}

document.addEventListener('DOMContentLoaded', function () {
  var default_selectors = d.querySelectorAll('.js_ghostGridHolder');
  for (var i = 0; i < default_selectors.length; i++) {
    showGrid(default_selectors[i], 12);
  }

  if (default_selectors.length) {
    var gridToggle = d.createElement('span');
    gridToggle.className = 'ghost-grid-toggle';
    kdxNavUtils.appendChild(gridToggle);

    if (JSON.parse(jsCookie.get('KDX_Tools_Grid_Opened') || 'false')) {
      for (var i = 0; i < default_selectors.length; i++) {
        toggleGrid(default_selectors[i]);
      }
      gridToggle.className = 'ghost-grid-toggle activated';
    }

    gridToggle.addEventListener('click', function () {
      for (var i = 0; i < default_selectors.length; i++) {
        toggleGrid(default_selectors[i]);
      }
      jsCookie.set('KDX_Tools_Grid_Opened', !JSON.parse(jsCookie.get('KDX_Tools_Grid_Opened') || 'false'), {expires: 1});
      gridToggle.className = gridToggle.className === 'ghost-grid-toggle' ? 'ghost-grid-toggle activated' : 'ghost-grid-toggle';
    })
  }

  function showGrid(el, columms) {
    var columns = columms || 12;
    var element = el || document.body;
    var grid = d.createElement('div');
    var rootfz = parseFloat(window.getComputedStyle(d.documentElement).fontSize);
    grid.className = 'ghost-grid';
    grid.innerHTML = '<div class="gridcontainer"><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div><div class="grid_1"><div class="ghost-grid__inner"></div></div></div>';
    element.style.position = window.getComputedStyle(element).getPropertyValue("position") === 'static' ? 'relative' : '';
    element.insertBefore(grid, element.firstChild);
    grid.style.width = (element.offsetWidth / rootfz) + 'rem';
    window.addEventListener('resize', function () {
      grid.style.width = (element.offsetWidth / rootfz) + 'rem';
    })
  }

  function toggleGrid(el) {
    el.style.visibility = !el.style.visibility ? 'hidden' : '';
  }
});

