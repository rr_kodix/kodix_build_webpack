# Сборщик проектов KDX
###Установка:
```bash
npm i
```
или
```bash
yarn
```

Во время установки будут созданы файл конфигурации и скрипты для `npm`
###Конфиг сборщика
Настройки для сборщика находятся в файле kdx.config.json, структура:

kdx.config.json [default]:
```js
{
  "default": { // Настройки по умолчанию
    "paths": { // Пути для сборки
      "sourceRoot": "src/",
      "pages": "src/pages/",
      "output": "build/",
      "publicPath": "/",
      "assets": {
        "js": "js/",
        "css": "css/",
        "fonts": "fonts/",
        "images": "images/",
        "templates": ""
      },
      "resolve": {
        // Тут можно задать алиасы для часто используемых файлов и директорий
        //"someAlias": "./src/some/path/to/some/nested/folder" // Далее, где необхоимо, вызываем алиас вместо пути к файлу @import "someAlias";
      },
      "hashes": false //все изображения, шрифты итд будут сохрянаться с hash
    },
    "kdxTools": false, // Меню, сетка и прочие фичи, полезные при разработке
    "devServer": { 
      "active": false, // По-умолчанию сервер не стартует
      "port": 8080 // Порт сервера, который можно переопределить ниже
    },
    "minify": false, // Минификация
    "provide": {
      // Добавление переменной в js
      //"name": "Homer" // Далее в любом js файле используем  console.log(name); 
    },    
    "externals": {}, // https://webpack.js.org/configuration/externals/
    "libs": [
      "jQuery" // Сюда можно положить библтотеки, которые обновляются нечасто и не тянуть их в при каждой пересборке
    ],
    "basisCss": "./src/globals/js/_basisCss.js" // Сбор базовых стилей для подключения в head (above the fold optimization), можно даже инлайново. Все остальное должно подключаться в конце body, чтобы Google PageSpeed Insights не ругался.
  },
  "development": { // Задача по умолчанию, запуск через npm run start
    "kdxTools": true, // Включаем dev-меню, сетку
    "devServer": {
      "active": true // Запускаем сервер
    }
  },
  "build": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=test
    "paths": {
      "publicPath": "/"
    },
    "kdxTools": true
  },
  "production": { // Задача, переопределяющая настройки по умолчанию, запуск через переменную NODE_ENV=production
    "paths": {
      "assets": {
        "templates": "templates/"
      }
    },
    "minify": true
  }
  // Можно создать кастомные задачи с нужными параметрами, запуск по имени узла в json, вызов также как и с задачами выше, через переменную NODE_ENV=your_task
}
```


### Скрипты
  #### Скрпты для unix систем имеют суффикс -unix
    
  **Запуск dev-cервера**
  ```
  npm run start
  ```
    
  **Запуск сборки статики для загрузки на сервер** _Прим. NODE_ENV=`build`_
  ```
  npm run build
  ```
    
  **Запуск сборки для Production** _Прим. NODE_ENV=`production`_
  ```
  npm run deploy
  ```

  **Запуск сборок в режиме слежения**
  Например:
  ```
  npm run deploy:watch
  ```
  
  **Генерация CHANGELOG'a**
  ```
  npm run changelog
  ```
